from django.contrib import admin
from app.models import Contest, Problem

admin.site.register(Contest)
admin.site.register(Problem)
# Register your models here.
