from django.db import models

# Create your models here.
class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.TextField()
    password = models.TextField()
    name = models.TextField()
    avatar = models.TextField()

    class Meta:
        db_table = 'user'

class Contest(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    start_time = models.BigIntegerField()
    duration = models.IntegerField()

    class Meta:
        db_table = 'contest'

class Problem(models.Model):
    id = models.AutoField(primary_key=True)
    contest = models.ForeignKey(Contest, on_delete=models.CASCADE)
    name = models.TextField()
    local_name = models.BigIntegerField()
    url = models.TextField()

    class Meta:
        db_table = 'problem'