# Generated by Django 3.2 on 2021-05-05 15:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contest',
            fields=[
                ('id', models.CharField(blank=True, max_length=8, primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('start_time', models.BigIntegerField()),
                ('duration', models.IntegerField()),
                ('status', models.TextField()),
            ],
            options={
                'db_table': 'contest',
            },
        ),
        migrations.AlterField(
            model_name='user',
            name='id',
            field=models.CharField(blank=True, max_length=8, primary_key=True, serialize=False),
        ),
        migrations.CreateModel(
            name='Problem',
            fields=[
                ('id', models.CharField(blank=True, max_length=8, primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('local_name', models.BigIntegerField()),
                ('url', models.TextField()),
                ('contest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.contest')),
            ],
            options={
                'db_table': 'problem',
            },
        ),
    ]
