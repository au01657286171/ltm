from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response
from app.models import Contest, Problem
from app.serializers import ContestSerializer

# Create your views here.
class ContestListView(generics.ListCreateAPIView):
    queryset = Contest.objects
    serializer_class = ContestSerializer

